--1-------------SELECT----------------
SELECT * FROM Product
SELECT * FROM Balance
SELECT * FROM Price where currency_code in ('EUR', 'GBP')
--2-------------SELECT WITH FILTER---------------
SELECT COUNT(product_type) AS Class1F_Products FROM Product where product_type='Class1F'
SELECT COUNT(product_type) AS Class1M_Products FROM Product where product_type='Class1M'

SELECT product_type, COUNT(*) AS ProductCount FROM Product WHERE product_type in ('Class1F', 'Class1M') GROUP BY product_type

--4----------------------------

DECLARE @ProductName NVARCHAR(200)
SET @ProductName = 'butter'
SELECT * FROM vw_ProductsClass1F1M WHERE name = @ProductName


CREATE INDEX IX_Product_Name ON Product (name); --this is my proposal

--5-------------CPU-----------------

WITH CPU
AS
(SELECT 
 dmpa.DatabaseID
 , DB_Name(dmpa.DatabaseID) AS [Database]
 , SUM(dmqs.total_worker_time) AS CPUTimeAsMS
 FROM sys.dm_exec_query_stats dmqs 
 CROSS APPLY 
 (SELECT 
 CONVERT(INT, value) AS [DatabaseID] 
 FROM sys.dm_exec_plan_attributes(dmqs.plan_handle)
 WHERE attribute = N'dbid') dmpa
 GROUP BY dmpa.DatabaseID)
 
 SELECT 
 [Database] 
 ,[CPUTimeAsMS] 
 ,CAST([CPUTimeAsMS] * 1.0 / SUM([CPUTimeAsMS]) OVER() * 100.0 AS DECIMAL(5, 2)) AS [CPUTimeAsPercent]
 FROM CPU
 ORDER BY [CPUTimeAsMS] DESC;

 --6-------------INDEX-----------------

SELECT 
    OBJECT_NAME(s.object_id) AS TableName,
    i.name AS IndexName,
    i.index_id AS IndexID,
    user_seeks + user_scans + user_lookups AS UserReads,
    user_updates AS UserWrites,
    last_user_seek AS LastUserSeek,
    last_user_scan AS LastUserScan,
    last_user_lookup AS LastUserLookup,
    last_user_update AS LastUserUpdate
FROM 
    sys.dm_db_index_usage_stats s
INNER JOIN 
    sys.indexes i ON s.object_id = i.object_id AND s.index_id = i.index_id
WHERE 
    database_id = DB_ID()
ORDER BY 
    UserReads DESC, UserWrites DESC;

--7----------------DELETE IN BATCHES---------------------

DECLARE @BatchSize INT = 100000;

WHILE 1 = 1
BEGIN
    DELETE TOP (@BatchSize)
    FROM VeryLargeLogTable
    WHERE DateColumn < DATEADD(YEAR, -5, GETDATE());

    IF @@ROWCOUNT = 0
        BREAK;

    COMMIT;
END
