## IF task
This is the task assigned to me for evaluation of my competence to work as Database Administrator


## SQL Query tasks

I've put Queries from questions 1-7 in a .sql file in this repository. For questions which need further explaining, answers are provided below.

#### Question 3

This has been skipped as I'm not capable of writing queries as such on my own.

#### Question 4

It is possible to improve query executing speeds by creating indexes. With this solution I've managed to reduce query execution duration by ~91%

![alt text](https://i.imgur.com/U1iC52S.png)

#### Question 7

Deleting in smaller batches minimizes the impact on system resources such as CPU, RAM and disk I/O, maintains acceptable performance for other database operations. Some locks might happen but that you can avoid by using WITH (NOLOCK).

#### Question 8

There might be many causes that can lead to timeouts. To help identify causes of timeout it might be worth to run SQL Profiler with Duration filter and monitor system hardware and checking the following:

 ##### 1. Heavy load due to unoptimized query
    
    This can be solved by optimizing the query which is being executed. Copy-and paste the query to a senior developer to determine if its optimised or not.

##### 2. Index

    Checking if index exists, shceduling a task which defragments it

##### 3. Hardware resources

    Visiting monitoring to check if there is resources deficit

##### 4. Suboptimal query execuiton plans

    Use query optimizer to generate efficient execution plans

##### 5. SQL Profiler

    Starting a trace

##### 6. Concurrent transactions in peak hours

    Related to insufficient hardware resources however if there is a significant increase of load in peak hours, considering dynamic scaling.

#### Question 9

I would consider using SSDs due to their superior I/O performance. In large-scale deployments or those with high performance requirements I would evaluate the possibility of using a SAN. Using RAID 10 is a great balance of performance and redundancy.

Placing data files and log files on seperate drives can bring a huge benefit to I/O.

The specific configuration will depend on estimated workload, size of data. 

#### Question 10

I believe this is somewhat answered in question 8 already, however, there's also possibility of doing some compression, using caching mechanisms. Database Engine Tuning Advisor might come in handy to analyze a workload and recieve indexing, statistics and partitioning improvement recommendations.

#### Question 11

There should be recent entries in SQL Server Logs which indicate to failed restore attempts. 

#### Question 12

There's a possibility of creating the backups with compression. You could split these backups in multiple files or even create backups on seperate disks. All of these solutions will improve database backup creation durations.

Also make sure the disk to which you are creating the backup has sufficient I/O performance. Considering that this is a storage in a network, even though it's local, there's a possibility of network performance issues. 

#### Question 13

1. Use covering indexes that include all columns.
2. Create indexes based on query patterns.
3. Optimize queries for efficiency.
4. Keep statistics up-to-date.
5. Consider denormalization for better read performance.
6. Implement query caching.
7. Partition large tables.


